import { NgModule, ErrorHandler } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';
import { IonicApp, IonicModule, IonicErrorHandler } from 'ionic-angular';
import { MyApp } from './app.component';
import { HomePage } from '../pages/home/home';
import { StatusBar } from '@ionic-native/status-bar';
import { SplashScreen } from '@ionic-native/splash-screen';
import { ChatService } from '../chat.service';
import { WebsocketService } from '../websocket.service';
import { ChatMessageComponent } from '../components/chat-message/chat-message.component';
import { MessageListComponent } from '../components/message-list/message-list.component'
import { UserInputComponent } from '../components/user-input/user-input.component';
import { ChatWindowComponent } from '../components/chat-window/chat-window.component';
import { ChatSettingsComponent } from '../components/chat-settings/chat-settings';
import { WindowProvider } from '../providers/window/window';

@NgModule({
  declarations: [
    MyApp,
    HomePage,
    MessageListComponent,
    UserInputComponent,
    ChatWindowComponent,
    ChatMessageComponent,
    ChatSettingsComponent
  ],
  imports: [
    BrowserModule,
    IonicModule.forRoot(MyApp)
  ],
  bootstrap: [IonicApp],
  entryComponents: [
    MyApp,
    HomePage,
    MessageListComponent,
    UserInputComponent,
    ChatWindowComponent,
    ChatMessageComponent,
    ChatSettingsComponent
  ],
  providers: [
    StatusBar,
    SplashScreen,
    ChatService,
    WebsocketService,
    {provide: ErrorHandler, useClass: IonicErrorHandler},
    WindowProvider
  ]
})
export class AppModule {}
