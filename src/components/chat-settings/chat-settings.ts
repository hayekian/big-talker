import { Component } from '@angular/core';

import { NavParams, ViewController } from 'ionic-angular';
import { NgForm } from '@angular/forms';

/**
 * Generated class for the ChatSettingsComponent component.
 *
 * See https://angular.io/api/core/Component for more info on Angular
 * Components.
 */
@Component({
  selector: 'chat-settings',
  templateUrl: 'chat-settings.html'
})
export class ChatSettingsComponent {

  alias: String;
  room: String;

  constructor(params: NavParams, public viewCtrl: ViewController) {
    this.alias = params.data.alias;
    this.room = params.data.room;
  }

  closeModal(save) {
    this.viewCtrl.dismiss({ alias: this.alias, room: this.room, save: save });
  }


  onSubmit(form: NgForm) {
    this.closeModal(true);
  }

}
