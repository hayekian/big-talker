import { Component, OnInit } from '@angular/core';
import { ChatSettingsComponent } from '../chat-settings/chat-settings';
import { ChatService } from '../../chat.service';
import { ModalController } from 'ionic-angular';
import { WindowProvider } from '../../providers/window/window';
@Component({
  selector: 'app-chat-window',
  templateUrl: './chat-window.component.html'
})
export class ChatWindowComponent implements OnInit {

  alias = 'testuser';
  room = 'lounge';

  messages = [];

  constructor(private chat: ChatService, public modalCtrl: ModalController, public window: WindowProvider) { }

  ngOnInit() {
    this.chat.messages.subscribe(msg => {
      console.log('got message');
      if (msg.type == 'newChannel') {
        this.messages = msg.messages;
        for (var i = 0; i < this.messages.length; i++)
          this.messages[i].mine = this.alias == this.messages[i].alias;    
      }
      else {
        msg.mine = msg.alias == this.alias;
        this.messages.unshift(msg);
      }
    });
    
    this.initChat();
  }


  btnChangeRoom() {

    const modal = this.modalCtrl.create(ChatSettingsComponent, { alias: this.alias, room: this.room });

    modal.onDidDismiss(data => {
      if (data.save && (data.alias != this.alias || data.room != this.room)) {
        this.alias = data.alias;
        this.room = data.room;
        this.initChat();
      }
      return false;
    });

    modal.present();

  }

  onUserMessage(evt) {
    console.log('sending message:'+ evt);
    let m = { message: evt };
    this.chat.sendMsg(m);
  }

  initChat() {
    let m = { room: this.room, alias: this.alias };
    this.chat.sendMsg(m);
  }
}
