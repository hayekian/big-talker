import { NgModule } from '@angular/core';
import { UserInputComponent } from './user-input/user-input.component';
import { ChatMessageComponent } from './chat-message/chat-message.component';
import { MessageListComponent } from './message-list/message-list.component';
import { ChatWindowComponent } from './chat-window/chat-window.component';
import { ChatSettingsComponent } from './chat-settings/chat-settings';

@NgModule({
  declarations: [ UserInputComponent, ChatMessageComponent, MessageListComponent, ChatWindowComponent,
    ChatSettingsComponent],
	imports: [],
  exports: [ UserInputComponent, ChatMessageComponent, MessageListComponent, ChatWindowComponent,
    ChatSettingsComponent]
})
export class ComponentsModule {}
