import { Component, OnInit, EventEmitter , Output} from '@angular/core';

@Component({
  selector: 'app-user-input',
  templateUrl: './user-input.component.html'
})
export class UserInputComponent implements OnInit {
  
  userInput: string;

 @Output() messageCreated = new EventEmitter<any>();


  constructor() {}

  ngOnInit() {
  }

  onSubmit() {
    this.messageCreated.emit(this.userInput);
    console.log(this.userInput);
    this.userInput = '';
  }


}
