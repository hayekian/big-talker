import { Injectable } from '@angular/core';
import { Observable } from 'rxjs/Observable';
import * as io from 'socket.io-client';
import * as Rx from 'rxjs/Rx';

@Injectable()
export class WebsocketService {

  // Our socket connection
  private socket;

  constructor() { }

  connect(): Rx.Subject<MessageEvent> {
    
    this.socket = io('http://localhost:5000');
   

    
    let observable = new Observable(observer => {


      this.socket.on('message', (data) => {
        observer.next(data);
      })
      return () => {
        this.socket.disconnect();
      }
    });

    
    let observer = {
      next: (data: Object) => {
        this.socket.emit('message', JSON.stringify(data));
      },
    };

    
    return Rx.Subject.create(observer, observable);
  }

}
